function FindProxyForURL(url, host) {

  var PROXY = "PROXY 3.14.138.48:14318";
  var DEFAULT = "DIRECT";

  if (/(faa.gov)/i.test(host)) return PROXY;
  return DEFAULT;
}
roo